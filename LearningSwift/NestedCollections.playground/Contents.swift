//: Playground - noun: a place where people can play

import UIKit


// # SIMPLE ARRAY


//  +--------+----------+----------+----------+
//  |        |  Column  |  Column  |  Column  |
//  | Matrix | Index(0) | Index(1) | Index(2) |
//  +--------+----------+----------+----------+
//  | Row(1) |     a    |     b    |     c    |
//  +--------+----------+----------+----------+

var array: [String] = ["a","b","c"]

// Count the number of items
array.count

// Return the first value of the array based on its index (0, 1, 2)
array[0]


// # MULTIDIMENSIONAL ARRAY


// +--------+--------------+------------+------------+
// |        |    Column    |   Column   |   Column   |
// | Matrix |   Index(0)   |  Index(1)  |  Index(2)  |
// +--------+--------------+------------+------------+
// | Row(1) | [10, 20, 30] | [40,50,60] | [70,80,90] |
// +--------+--------------+------------+------------+

var arrayMulti: [[Int]] = [[10,20,30],[40,50,60],[70,80,90]]

// Return the number of sub arrays of *arrayMulti*
arrayMulti.count

// Return a specific sub-array such the second array at column/index 1.
arrayMulti[1]

// Return a specific value from *arrayMulti* such as 50
// Step 1 "Selecting the sub-array": arrayMulti[1] -> [40,50,60]
// Step 2 "Selecting the value in the sub-array": arrayMulti[1][1] -> 50
//
// +--------+--------------+-----------------------------------------+------------+
// |        |    Column    |                  Column                 |   Column   |
// | Matrix |   Index(0)   |                 Index(1)                |  Index(3)  |
// +--------+--------------╬════════╦══════════╦══════════╦══════════╬------------+
// |        |              ║   Sub  ║  Column  ║  Column  ║  Column  ║            |
// |        |              ║ Matrix ║ Index(0) ║ Index(1) ║ Index(2) ║            |
// +        +              ╬════════╬══════════╬══════════╬══════════╣            +
// | Row(1) | [10, 20, 30] ║ Row(1) ║    40    ║  -> 50   ║    60    ║ [70,80,90] |
// +--------+--------------╩════════╩══════════╩══════════╩══════════╩------------+

arrayMulti[1][1]

// Return each value of the array
// i = from index 0 to value less than 3 (arrayMulti.count). ArrayMulti only has indexes 0, 1, 2 (NOT 3).
for row in 0..<arrayMulti.count {
  for column in 0..<arrayMulti[row].count {
   arrayMulti[row][column]
    break
  }
  
// # MULTIDIMENSIONAL ARRAY WITH NESTED ARRAYS OF DIFFERENT TYPES
var superHeroes: [String: [String: AnyObject]] = [
    "Batman": ["Real name": "Bruce Wayne", "First appearance": 1939, "City": "Gotham", "Power": "Intelligence"],
    "Superman": ["Real name": "Clark Kent", "First appearance": 1938, "City": "Metropolis", "Power": "Strength"],
    "Flash": ["Real name":  "Barry Allen", "First appearance": 1940, "City": "Central", "Power": "Speed"]
  ]

// Return the number of super heroes
let batmanCity = superHeroes["Batman"]!["City"]!
print("Who is protecting \(batmanCity) city?")

// Returning (String) keys of Superheroes
// Memo: Dictionary = (Key: Value)
  
  var superHeroesNames: [String] = Array(superHeroes.keys)

// Write a sentence describing the main power of each super hero and add them to an array.
  var superHeroesPowers: [String] = [""]
  for i in 0..<superHeroesNames.count {
    let superHeroName = superHeroesNames[i]
    let sentence = "\(superHeroName)'s main power is \(superHeroes[superHeroName]!["Power"]!)!"
   superHeroesPowers.append(sentence)
  }
  
  print(superHeroesPowers)
}












